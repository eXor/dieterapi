﻿(function ($scope) {
    'use strict';

    angular.module("xPlat.controllers").controller("BumpCtrl", ['$scope', 'maps', 'localStorage', BumpCtrl]);

    /**
	 * Controller for the BumpApp list.
	 * 
	 * @param {!angular.Service} maps
	 * @param {!angular.Service} storage
	 * @constructor
	 * @export
	 */
    function BumpCtrl($scope, maps, localStorage) {
        var bumpCtrl = this;

        bumpCtrl.test = 'hallo D!';
        bumpCtrl.maps = maps;
        
        bumpCtrl.acceleroWatch;
        bumpCtrl.geolocationWatch;

        // Wait for device API libraries to load
        //
        document.addEventListener("deviceready", onDeviceReady, false);

        // device APIs are available
        //
        function onDeviceReady() {
            navigator.accelerometer.getCurrentAcceleration(bumpCtrl.getAccData, function () { });
        }

        bumpCtrl.start = function () {
            var options = { frequency: 50 };

            bumpCtrl.acceleroWatch = navigator.accelerometer.watchAcceleration(bumpCtrl.getAccData, function () { }, options);
            bumpCtrl.geolocationWatch = navigator.geolocation.watchPosition(bumpCtrl.getGeolocation, function () { }, options);
        }

        bumpCtrl.getAccData = function (acceleration) {
            $scope.acc = acceleration;

            var accelero = {
                x: acceleration.x,
                y: acceleration.y,
                z: acceleration.z
            }
            var geo = {};
            var timestamp = acceleration.timestamp;
            
            localStorage.create(accelero, geo, timestamp);
        }

        bumpCtrl.getGeolocation = function (geolocation) {
            $scope.geo = geolocation;
            var accelero = {};
            var geo = geolocation.coords;
            
            var timestamp = geolocation.timestamp;

            localStorage.create(accelero, geo, timestamp);      
        }

        bumpCtrl.stop = function () {
            alert('stopped');
            navigator.accelerometer.clearWatch(bumpCtrl.getAccData);
            navigator.geolocation.clearWatch(bumpCtrl.getGeolocation);
        }
    }

    BumpCtrl.prototype.stop = function () {
        alert('Stopped');
        return;
    }

    BumpCtrl.prototype.updateSample = function (sample) {
        var _this = this;

        return this.maps.getCurrentPosition()
            .then(_this.maps.getAddressFromPosition.bind(_this.maps), function (error) { return error.message; })
            .then(function (address) {
                sample.address = address;
                return _this.storage.update(sample);
            }, function (errorMessage) {
                sample.address = errorMessage;
                return _this.storage.update(sample);
            });
    };


})();